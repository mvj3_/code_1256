private Button button1; 
	private Button button2;
	private LinearLayout container;
	private OnClickListener l = new OnClickListener(){
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch(v.getId()){
			case R.id.button1:
				switchActivity(0);
				break;
			case R.id.button2:
				switchActivity(1);
				break;
			}
		}
	};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		button1 = (Button)findViewById(R.id.button1);
		button2 = (Button)findViewById(R.id.button2);
		container = (LinearLayout) findViewById(R.id.container);
		button1.setOnClickListener(l);
		button2.setOnClickListener(l);
		switchActivity(0);
	}
	private void switchActivity(int id){
		container.removeAllViews();
		Intent intent = null;
		switch(id){
		case 0:
			intent = new Intent(this,TestActivity1.class);
			break;
		case 1:
			intent = new Intent(this,TestActivity2.class);
			break;
		}
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		Window  subActivity = getLocalActivityManager().startActivity("subActivity", intent);
		container.addView(subActivity.getDecorView(),LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
	}